package com.vti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vti.entity.Account;

public interface IAccountRepository extends JpaRepository<Account, Integer>,JpaSpecificationExecutor<Account> {
	
	@Modifying
	@Transactional
	@Query("DELETE FROM Account a WHERE id IN(:ids)")
	public void deleteAccounts(@Param("ids") List<Integer> ids);
	
	public Account findByUsername(String username);

	public boolean existsByUsername(String username);

}
