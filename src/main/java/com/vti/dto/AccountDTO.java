package com.vti.dto;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccountDTO extends RepresentationModel<AccountDTO>{

	private int id;
	
	private String username;
	
	
	private String firstName;
	
	
	private String lastName;
	

	private String role;
//	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
//	private Date createDate;
	
	private int departmentId;

	@JsonProperty("department")
	private String departmentName;
	
	
	
}

