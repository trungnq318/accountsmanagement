package com.vti.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vti.dto.DepartmentDTO;
import com.vti.entity.Department;
import com.vti.form.department.CreatingDepartmentForm;
import com.vti.form.department.DepartmentFilterForm;
import com.vti.form.department.UpdatingDepartmentForm;
import com.vti.service.IDepartmentService;
import com.vti.validation.department.DepartmentIDExists;

@RestController
@RequestMapping(value = "api/v1/departments")
@Validated
public class DepartmentController {
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private IDepartmentService service;

	@GetMapping()
	public Page<DepartmentDTO> getAllDepartments(Pageable pageable,
			@RequestParam(value="search",required = false) String search,
			DepartmentFilterForm form) {
		Page<Department> entityPages = service.getAllDepartments(pageable,search,form);

		// convert entities --> dtos
		List<DepartmentDTO> dtos = modelMapper.map(
				entityPages.getContent(), 
				new TypeToken<List<DepartmentDTO>>() {}.getType());
		
		for (DepartmentDTO departmentDTO : dtos) {
			departmentDTO.add(linkTo(methodOn(DepartmentController.class).getDepartmentByID(departmentDTO.getId())).withSelfRel());
			for (DepartmentDTO.AccountDTO accountDTO :departmentDTO.getAccounts() ) {
				accountDTO.add(linkTo(methodOn(AccountController.class).getAccountByID(accountDTO.getId())).withSelfRel());
			}
		}
		Page<DepartmentDTO> dtoPages = new PageImpl<>(dtos, pageable, entityPages.getTotalElements());

		return dtoPages;
	}

	@GetMapping(value = "/{id}")
	public DepartmentDTO getDepartmentByID(@PathVariable(name = "id") @DepartmentIDExists int id) {
		Department entity = service.getDepartmentByID(id);

		// convert entity to dto
		DepartmentDTO dto = modelMapper.map(entity, DepartmentDTO.class);
		
		// add Hateoas
		dto.add(linkTo(methodOn(DepartmentController.class).getDepartmentByID(id)).withSelfRel());

		return dto;
	}
	@GetMapping(value="/exists/{name}")
	public boolean existsByUsername(@PathVariable(name = "name")  String name) {

		return service.isDepartmentExistsByName(name);
	}
	
	@PostMapping()
	public void createDepartment(@RequestBody @Valid CreatingDepartmentForm form) {
		service.createDepartment(form);
	}
	@PutMapping(value="/{id}")
	public void updateDepartment(@PathVariable int id,@RequestBody @Valid UpdatingDepartmentForm form) {
		service.updateDepartment(id,form);
	}
	@DeleteMapping(value="/{id}")
	public void deleteDepartment(@PathVariable int id) {
		service.deleteDepartment(id);
	}
	
	@DeleteMapping
	public void deleteDepartments(@RequestParam(name = "ids") List<Integer> ids) {
		service.deleteDepartments(ids);
	}

}
