package com.vti.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vti.dto.AccountDTO;
import com.vti.entity.Account;
import com.vti.form.AccountFilterForm;
import com.vti.form.CreatingAccountForm;
import com.vti.form.UpdatingAccountForm;
import com.vti.service.IAccountService;
import com.vti.validation.account.AccountIDExists;

@RestController
@RequestMapping(value = "api/v1/accounts")
@Validated
public class AccountController {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private IAccountService accountService;

	
	@GetMapping()
	public Page<AccountDTO> getAllAccounts(Pageable pageable,
			@RequestParam(value="search",required = false) String search,
			AccountFilterForm form) {

		Page<Account> entityPages = accountService.getAllAccounts(pageable,search,form);

		// convert entities --> dtos
		List<AccountDTO> dtos = modelMapper.map(
				entityPages.getContent(), 
				new TypeToken<List<AccountDTO>>() {}.getType());
		
		for (AccountDTO accountDTO : dtos) {
			accountDTO.add(linkTo(methodOn(AccountController.class).getAccountByID(accountDTO.getId())).withSelfRel());
		}
		Page<AccountDTO> dtoPages = new PageImpl<>(dtos, pageable, entityPages.getTotalElements());

		return dtoPages;
	}
	@GetMapping(value="/exists/{username}")
	public boolean existsByUsername(@PathVariable(name = "username")  String username) {

		return accountService.isAccountExistsByUsername(username);
	}

	@GetMapping(value = "/{id}")
	public AccountDTO getAccountByID(@PathVariable(name = "id") @AccountIDExists int id) {

		Account entity = accountService.getAccountByID(id);

		// convert entity to dto
		AccountDTO dto = modelMapper.map(entity, AccountDTO.class);
		
		// add Hateoas
		dto.add(linkTo(methodOn(AccountController.class).getAccountByID(id)).withSelfRel());

		return dto;
	}
	@PostMapping()
	public void createAccount(@RequestBody @Valid CreatingAccountForm form) {
		accountService.createAccount(form);
	}
	@PutMapping(value="/{id}")
	public void updateAccount(@PathVariable int id,@RequestBody @Valid UpdatingAccountForm form) {
		accountService.updateAccount(id,form);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteAccount(@PathVariable int id) {
		accountService.deleteAccount(id);
	}
	
	@DeleteMapping
	public void deleteAccounts(@RequestParam(name = "ids") List<Integer> ids) {
		accountService.deleteAccounts(ids);
	}
}
