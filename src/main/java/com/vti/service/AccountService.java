package com.vti.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vti.entity.Account;
import com.vti.form.AccountFilterForm;
import com.vti.form.CreatingAccountForm;
import com.vti.form.UpdatingAccountForm;
import com.vti.repository.IAccountRepository;
import com.vti.specification.AccountSpecification;

@Service
@Transactional
public class AccountService implements IAccountService {

	@Autowired
	private IAccountRepository accountRepository;
	@Autowired
	private ModelMapper modelMapper;

//	public List<Account> getAllAccounts() {
//		return accountRepository.findAll();
//	}
	public Page<Account> getAllAccounts(Pageable pageable,String search, AccountFilterForm form) {
		Specification<Account> where= AccountSpecification.buildWhere(search,form);
		return accountRepository.findAll(where,pageable);
	}
	@Override	
	public Account getAccountByID(int id) {
		return accountRepository.findById(id).get();
	}

	@Override
	public void createAccount(CreatingAccountForm form) {
		// omit id field
		TypeMap<CreatingAccountForm, Account> typeMap = modelMapper.getTypeMap(CreatingAccountForm.class,
				Account.class);
		if (typeMap == null) { // if not already added
			// skip field
			modelMapper.addMappings(new PropertyMap<CreatingAccountForm, Account>() {
				@Override
				protected void configure() {
					skip(destination.getId());
				}
			});
		}

		Account account = modelMapper.map(form, Account.class);
		accountRepository.save(account);
		
	}

	@Override
	public void updateAccount(int id, UpdatingAccountForm form) {
		Account account = modelMapper.map(form, Account.class);
		account.setId(id);
		accountRepository.save(account);
		
	}

	@Override
	public void deleteAccount(int id) {
		accountRepository.deleteById(id);
		
	}

	@Override
	public void deleteAccounts(List<Integer> ids) {
		accountRepository.deleteAccounts(ids);
		
	}
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account=accountRepository.findByUsername(username);
		if (username==null) {
			throw new UsernameNotFoundException(username);
		}
		UserDetails userDetails=new User(account.getUsername(), account.getPassword(), AuthorityUtils.createAuthorityList(account.getRole().toString()));
		return userDetails;
	}
	@Override
	public Account getAccountByUsername(String username) {
		// TODO Auto-generated method stub
		return accountRepository.findByUsername(username);
	}
	@Override
	public boolean isAccountExistsByUsername(String username) {
		// TODO Auto-generated method stub
		return accountRepository.existsByUsername(username);
	}
	@Override
	public boolean isAccountExistsByID(Integer id) {
		// TODO Auto-generated method stub
		return accountRepository.existsById(id);
	}

	

}
