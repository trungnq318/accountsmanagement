package com.vti.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vti.entity.Account;
import com.vti.entity.Department;
import com.vti.form.department.CreatingDepartmentForm;
import com.vti.form.department.DepartmentFilterForm;
import com.vti.form.department.UpdatingDepartmentForm;
import com.vti.repository.IDepartmentRepository;
import com.vti.specification.department.DepartmentSpecification;

@Service
@Transactional
public class DepartmentService implements IDepartmentService {
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private IDepartmentRepository repository;

	public Page<Department> getAllDepartments(Pageable pageable,String search,
			DepartmentFilterForm form) {
		Specification<Department> where= DepartmentSpecification.buildWhere(search,form);
		return repository.findAll(where,pageable);
	}

	public Department getDepartmentByID(int id) {
		return repository.findById(id).get();
	}

	@Override
	public boolean isDepartmentExistsByID(Integer id) {
		// TODO Auto-generated method stub
		return repository.existsById(id);
	}

	@Override
	public boolean isDepartmentExistsByName(String name) {
		// TODO Auto-generated method stub
		return repository.existsByName(name);
	}

	@Override
	public void createDepartment(CreatingDepartmentForm form) {
		// omit id field
				TypeMap<CreatingDepartmentForm, Department> typeMap = modelMapper.getTypeMap(CreatingDepartmentForm.class,
						Department.class);
				if (typeMap == null) { // if not already added
					// skip field
					modelMapper.addMappings(new PropertyMap<CreatingDepartmentForm, Department>() {
						@Override
						protected void configure() {
							skip(destination.getId());
						}
					});
				}

				Department department = modelMapper.map(form, Department.class);
				repository.save(department);
	}

	@Override
	public void updateDepartment(int id, UpdatingDepartmentForm form) {
		Department department = modelMapper.map(form, Department.class);
		department.setId(id);
		repository.save(department);
		
	}

	@Override
	public void deleteDepartment(int id) {
		repository.deleteById(id);
		
	}

	@Override
	public void deleteDepartments(List<Integer> ids) {
		repository.deleteDepartments(ids);
		
	}
}
