package com.vti.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.vti.entity.Department;
import com.vti.form.department.CreatingDepartmentForm;
import com.vti.form.department.DepartmentFilterForm;
import com.vti.form.department.UpdatingDepartmentForm;

public interface IDepartmentService {

	public Page<Department> getAllDepartments(Pageable pageable,String search,
			DepartmentFilterForm form);

	public Department getDepartmentByID(int id);

	public boolean isDepartmentExistsByID(Integer id);

	public boolean isDepartmentExistsByName(String name);

	public void createDepartment(CreatingDepartmentForm form);

	public void updateDepartment(int id, UpdatingDepartmentForm form);

	public void deleteDepartment(int id);

	public void deleteDepartments(List<Integer> ids);
}
