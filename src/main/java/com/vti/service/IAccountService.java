package com.vti.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.vti.entity.Account;
import com.vti.form.AccountFilterForm;
import com.vti.form.CreatingAccountForm;
import com.vti.form.UpdatingAccountForm;

public interface IAccountService extends UserDetailsService {

//	public List<Account> getAllAccounts();

	public Account getAccountByID(int id);
	
	public void createAccount(CreatingAccountForm form);

	public void updateAccount(int id, UpdatingAccountForm form);

	public void deleteAccount(int id);

	public void deleteAccounts(List<Integer> ids);

	public Page<Account> getAllAccounts(Pageable pageable,String search, AccountFilterForm form);

	public Account getAccountByUsername(String username);

	public boolean isAccountExistsByUsername(String username);

	public boolean isAccountExistsByID(Integer id);
}
