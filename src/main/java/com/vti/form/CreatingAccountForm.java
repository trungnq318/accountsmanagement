package com.vti.form;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vti.validation.account.AccountUsernameNotExists;
import com.vti.validation.department.DepartmentIDExists;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreatingAccountForm {
	
	@NotBlank(message = "The username mustn't be null value")
	@Length(max = 50, min = 6, message = "The name's length is max 50 characters and min 6 characters")
	@AccountUsernameNotExists
	private String username;
	
	@NotBlank(message = "The firstName mustn't be null value")
	@Length(max = 50, message = "The name's length is max 50 characters")
	private String firstName;
	
	@NotBlank(message = "The lastName mustn't be null value")
	@Length(max = 50, message = "The name's length is max 50 characters")
	private String lastName;
	
	@Pattern(regexp = "ADMIN|EMPLOYEE|MANAGER", message = "The ROLE must be ADMIN|EMPLOYEE|MANAGER")
	private String role;
	
	
	@JsonProperty("department")
	@NotNull(message = "The department Id mustn't be null value")
	@DepartmentIDExists
	private int departmentId;
}
