package com.vti.form.department;

import javax.validation.constraints.Pattern;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UpdatingDepartmentForm {

//	private int id;
	
//	private String name;
//
//	private int totalMember;
	
	@Pattern(regexp = "DEV|TEST|PM|ScrumMaster", message = "The type must be DEV, TEST,ScrumMaster or PM")
	private String type;

}
