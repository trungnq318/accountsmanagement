package com.vti.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vti.validation.department.DepartmentIDExists;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UpdatingAccountForm {
	
	@Pattern(regexp = "ADMIN|EMPLOYEE|MANAGER", message = "The ROLE must be ADMIN|EMPLOYEE|MANAGER")
	private String role;
	
	@NotNull(message = "The name mustn't be null value")
	@DepartmentIDExists
	@JsonProperty("department")
	private int departmentId;
}
